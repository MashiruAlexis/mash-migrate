<?php
/**
 *	Clearance Categorization Proccessor
 *	@author mashirualexis
 */





// $_productCollection = Mage::getModel('catalog/product')->getCollection()->addAttributeToFilter('special_price', array('gt' => 1));
// foreach ($_productCollection as $_product) {
// 	$categories = $_product->getCategoryIds();
// 	$categories[] = $categoryId;
// 	$_product->setCategoryIds($categories);
// 	$_product->save();
// }
?>

<?php


Class cat {

	/**
	 *	Clearance Category ID
	 */
	public $parentCatId = 940;

	/**
	 *	Magento Mage File Path
	 */
	public $mageFilePath = "../new.lioncityco/app/Mage.php";

	/**
	 *	Get Parent ID
	 */
	public function getParentId() {
		return $this->parentCatId;
	}
	/**
	 *	Get Magento File Path
	 */
	public function getMageFilePath() {
		return $this->mageFilePath;
	}

	/**
	 *	Start Magento Execution
	 */
	public function execMage($acct = null) {
		require $this->mageFilePath;
		umask(0);
		if($acct) {
			Mage::app($acct);
		}
	}

	/**
	 *	Boot UP
	 */
	public function __construct() {
		include "core.php";
		ini_set('memory_limit', '-1');
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		set_time_limit(25000);
		error_reporting(E_ALL);
	}


	/**
	 *	Create Sub Category
	 */
	public function _createCat($catName) {
		$category = Mage::getModel('catalog/category');
		$category->setName($catName)
		->setIsActive(1)                       //activate your category
		->setDisplayMode('PRODUCTS')
		->setIsAnchor(1)
		->setCustomDesignApply(1)
		->setAttributeSetId($category->getDefaultAttributeSetId());
		
		$parentCategory = Mage::getModel('catalog/category')->load($this->parentCatId);
		$category->setPath($parentCategory->getPath());
		
		$category->save();
		unset($category);
	}

	/**
	 *	Get Products from a category
	 */
	public function getProd($catId) {
		return $products = Mage::getModel('catalog/category')->load($catId)
 		->getProductCollection()
 		->addAttributeToSelect('*') // add all attributes - optional
 		// ->addAttributeToFilter('status', 1) // enabled
 		->addAttributeToFilter('visibility', 4) //visibility in catalog,search
 		->setOrder('price', 'ASC'); //sets the order by price
	}

	/**
	 *	Get Category
	 */
	public function getCategory($catId) {
		return $products = Mage::getModel('catalog/category')->load($catId);
	}

	/**
	 *	Get Category by name
	 */
	public function getCatName($catName) {
		return $category = Mage::getResourceModel('catalog/category_collection')
    				->addFieldToFilter('name', $catName)
    				->getFirstItem() // The parent category
        			->getChildrenCategories(); // The child category
	}

	/**
	 *	Get All Products from Category
	 */
	public function getCatProd() {
		return Mage::getResourceModel('catalog/product_collection')->addCategoryFilter($this->getCategory($this->parentCatId));
	}

	/**
	 *	Get Child Categoy
	 */
	public function getChild() {
		$category = Mage::getModel('catalog/category')->load($this->parentCatId)->getAllChildren(true);
		if (count($category) > 0){
		    return $this->unsetArr($category, $this->parentCatId);
		}
	}
	
	/**
	 *	Get Child Names
	 */
	public function getChildNames() {
		foreach($this->getChild() as $chd) {
			$names[$this->getCategory($chd)->getId()] .= $this->getCategory($chd)->getName();
		}
		return $names;
	}
	
	/**
	 *	Get Child id by name
	 */
	public function getChildNameById($varChild) {
		
	}
	
	/**
	 *	Check if it exist
	 */
	public function dblCheck($varChk, $nameChk) {
		foreach($varChk as $chk) {
			if($this->getCategory($chk)->getName() == $nameChk) {
				return true;
			}
		}
		return false;
	}

	/**
	 *	Unset Array Value
	 */
	public function unsetArr($arr, $arrVal) {
		if($key = array_search($arrVal, $arr)) {
		    unset($arr[$key]); 
		}
		return $arr;
	}

	/**
	 *	Delete All subcategories
	 */
	public function _delCategory($catChild) {
		foreach($catChild as $newChild) {
			if($newChild == $this->parentCatId) {continue;}
			if($this->getCategory($newChild)->delete()) {
				echo "Category ID: " . $newChild . " -DELETED<br/>";
			}else{
				echo "Unable to delete. -" . $newChild . "<br/>";
			}
		}
	}
	
	/**
	 *	Get Category ID by Name
	 */
	public function getCatByName($varName = null) {
		$category = Mage::getResourceModel('catalog/category_collection')
    		->addFieldToFilter('name', 'Clearance')
    		->getFirstItem() // The parent category
        	->getChildrenCategories()
        	->addFieldToFilter('name', $varName)
        	->getFirstItem(); // The child category
		if($category->getId()) {
			return $category->getId();
		}
		return false;
	}

}

$cat = new cat;

$testCount = 0;
$cat->execMage("admin");
//$cat->_delCategory($cat->getChild());exit;
//echo "<pre>";
//print_r($cat->getChildNames());
//echo "</pre>";
//exit;
foreach($cat->getCatProd() as $catVals) {
$product_id = $catVals->getId();
$product = Mage::getModel('catalog/product')->load($product_id);
$cats = $cat->unsetArr($product->getCategoryIds(), $cat->getParentId());
foreach ($cats as $category_id) {
    $category = Mage::getModel('catalog/category')->load($category_id);
    $path = $category->getPath();
    $pathParts = explode('/', $path);
	$finalCatName = null;
    if (count($pathParts) == 3) {
		if(!in_array($category->getId(), $cat->getChild())) {
			
			if(!$cat->dblCheck($cat->getChild(), $category->getName())) {
				$cat->_createCat($category->getName());
			}
			$finalCatName = $category->getName();
		}
    }
    elseif (isset($pathParts[2])) {
        $topCategory = Mage::getModel('catalog/category')->load($pathParts[2]);
		if(!in_array($topCategory->getId(), $cat->getChild())) {
			if(!$cat->dblCheck($cat->getChild(), $topCategory->getName())) {
				$cat->_createCat($topCategory->getName());
			}
			$finalCatName = $topCategory->getName();
		}
		
    }
}
	foreach($cat->getChildNames() as $key => $varKey) {
		if($varKey == $finalCatName) {
			$categories = $catVals->getCategoryIds();
			$categories[] = $key;
			$catVals->setCategoryIds($categories);
			$catVals->save();
		}
	}
	
}
?>

<script>
//   $(document).ready(function(){
//     alert('Success.');
//   });
</script>