<?php
include "core.php";
ini_set('memory_limit', '-1');
set_time_limit(9000);

if(!$_SESSION['all_page_go'] == 1 or !isset($_SESSION['all_page_go'])){
	error_msg("Error, Connect to database first.");
	exit();
}

$conn_osc = mysqli_connect($_SESSION['osc_host'], $_SESSION['osc_user'], $_SESSION['osc_password'], $_SESSION['osc_database']);
if(!$conn_osc){echo "Error Connecting to database.". mysqli_error($conn_osc);} else { echo "Connected!"; }

$_address = 0;
$sql_get_products = "SELECT products_model, products_msrp, products_price from products where products_status = '1'";
$sql_run_products = mysqli_query($conn_osc, $sql_get_products);
if(mysqli_num_rows($sql_run_products)){
	while($sql_row_products = mysqli_fetch_array($sql_run_products)){
		if(!empty($sql_row_products['products_model'])){
			$prod_sku[$_address] = $sql_row_products['products_model'];
			$prod_msrp[$_address] = $sql_row_products['products_msrp'];
			$prod_price[$_address] = $sql_row_products['products_price'];
			$_address++;
		}
		
	}
}
ini_set("error_reporting",E_ALL);
ini_set("display_errors",true);
require_once MAGE_ADDRESS;
umask(0);
Mage::app('admin');

disp_msg($_address);
for($x = 0; $x < $_address; $x++){
	disp_msg("______________________________________________SKU: ".$prod_sku[$x]." _____________________________________________");
	$_product = Mage::getModel('catalog/product')->loadByAttribute('sku',$prod_sku[$x]);
	if($_product){
		$_product->setMsrp($prod_msrp[$x]);
		disp_msg("Product No.: ". $x);
		disp_msg("Store Price: ".$prod_price[$x]);
		disp_msg("Manufacturer SRP: ".$prod_msrp[$x]);
		if($_product->save()){
			success_msg("Success");
			$_product = "";
		}else{
			error_msg("Failed");
		}
	}
	
}
if($x == $_address){
	success_msg("Success in Migrating all MSRP");

}

?>
<script>
  $(document).ready(function(){
    alert('Done! Migrating :D');
  });
</script>