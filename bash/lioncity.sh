#!/bin/bash
clear

function headerName {

   echo "//////////////////////////"
   echo "//Backup Lioncityco Site//"
   echo "//////////////////////////" 
   
}

headerName

timeNow="$(date +"%I:%M:%S")"
dateNow="$(date +"%m-%d-%Y")"
backupFileName="lioncityco"
backupFileDirectory="/var/www/html/sites/mash-migrate.com/bash/backups/"
toBeBackupFileDirectory="/var/www/html/sites/mash-migrate.com/bash/test1"

echo "Please Choose number."

echo "1. Backup with details"
echo "2. Just Backup"
echo "3. Exit"
read choice

if [ $choice -eq 1 ] ; then
    
    clear
    headerName
    echo "You have choosen Backup with details."
    echo "With this selection we will ask some information about this backup."
    
    read -p "Would you like to add a date with in the filename? [y/n]: " dateFileName
    read -p "Would you like to add a note with this backup? [y/n]: " noteBool
    
    if [ $noteBool = y ] ; then
        
        read -p "Enter your note: " noteBody
        
    fi
    
    if [ $dateFileName = y ] ; then
        
        finalFileBackup="$toBeBackupFileDirectory $backupFileDirectory$backupFileName[$timeNow][$dateNow]"
        echo "Starting to Backup File"
        cp -a $finalFileBackup
        echo $noteBody > $backupFileDirectory$backupFileName[$timeNow][$dateNow]/readme
        echo "Done."
    
    else
        
        finalFileBackup="$toBeBackupFileDirectory $backupFileDirectory$backupFileName"
        echo "Starting to Backup File"
        cp -a $finalFileBackup
        
        if [ $noteBool = y ] ; then
        
            echo $noteBody > $backupFileDirectory$backupFileName[$timeNow][$dateNow]/readme
        
        fi
        
        echo "Done."
        
    fi
        
 fi   
   
    
if [ $choice -eq 2 ]; then
    
    finalFileBackup="$toBeBackupFileDirectory $backupFileDirectory$backupFileName[$timeNow][$dateNow]"
    clear
    headerName
    echo "With this selection we will start to backup without any information needed."
    echo "Starting Backup Proccess."
    cp -a $finalFileBackup
    echo "Done."

fi

if [ $choice -ge 3 ] ; then
    clear
    exit
fi