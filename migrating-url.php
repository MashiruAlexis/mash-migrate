<?php
include "core.php";
set_time_limit(6000);
?>
<!DOCTYPE html>
<html>
	<head>
		<title><?php echo TITLE_SITE; ?></title>
		<?php include "includes.php"; ?>
	</head>
<body>
<?php include "main_navbar.php"; ?>
<div class = "container">
	<div class="page-header">
		<h3>Migration</h3>
	</div>
<div class = "col-sm-6">
	<h3>OsCommerce</h3>
</div>
<div class="col-sm-12">
<h3>Migrating, please wait...</h3>
<?php
$conn_osc = mysqli_connect($_SESSION['osc_host'], $_SESSION['osc_user'], $_SESSION['osc_password'], $_SESSION['osc_database']);
if(!$conn_osc){echo "Error Connecting to database.". mysqli_error($conn_osc);}

$sql_get_products = "SELECT products_id, products_model from products where products_status = '1'";
$sql_run_products = mysqli_query($conn_osc, $sql_get_products);
$sku_address = 0;
if(mysqli_num_rows($sql_run_products) > 0){
	while($row_get_products = mysqli_fetch_array($sql_run_products)){
		$sku_product[$sku_address] = $row_get_products['products_model'];
		$id_product[$sku_address] = $row_get_products['products_id'];
		$sku_address++;
	}
}
require_once MAGE_ADDRESS;
Mage::app();
$null_sku = 0;
if($_createFile = fopen(".htaccess", "w")){
	for ($x = 0; $x <= count($sku_product); $x++){
		$url =  Mage::getModel('catalog/product')->loadByAttribute('sku',$sku_product[$x]);
			if(!$url->url_path == ""){
				$url = "Redirect 301 /product_info.php?products_id=".$id_product[$x]. " /http://www.new.lioncityco.com/".$url->url_path."\n";
				echo $url."<br/>";
				fwrite($_createFile, $url);
			}else{
				$null_sku++;
			}
	}	
}
$_lastLine = "RedirectMatch 301 .* https://www.lioncityco.com/";
fwrite($_createFile, $_lastLine);
echo info_msg(count($sku_product). " URL's was Successfully maped.");
echo "<br/>".$null_sku. " no sku products.";
fclose($_createFile);
?>
</div> <!-- Container Closing -->
</body>
</html>