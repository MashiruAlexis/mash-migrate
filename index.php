﻿<?php
include "core.php";
$pathCurrent = getcwd();
$filesInPath = scandir($pathCurrent);


foreach ($filesInPath as $files) {
	#echo "<br/>" . $files;
}
#echo "<br/>" . $pathCurrent;

function this_input($var_input){		
	if(!empty($var_input)){
		echo $var_input;
	}else{
		echo "";
	}
}

$err_message_body="";
$err_msg_int=0;
$succ_msg_int=0;
if(isset($_POST['submit'])){
	if(empty($_POST['osc_host']) or empty($_POST['osc_user'])  or empty($_POST['osc_database']) or empty($_POST['magento_host']) or empty($_POST['magento_user'])  or empty($_POST['magento_database'])){
		$err_message_body[$err_msg_int] = "Some field(s) needed are null, please review your input(s) and try again.";
		$err_msg_int++;
		$err_message_body[$err_msg_int] = "Note: You can leave password blank since we are in the developement stage and we may use localhost as our server.";
		$err_msg_int++;
		$err_message_body[$err_msg_int] = "Note: Some localhost server arent configured with password, until this script is done will have to let blank password for now.";
		$err_msg_int++;
	}else{
		$conn_osc = @new mysqli($_POST['osc_host'], $_POST['osc_user'], $_POST['osc_password'], $_POST['osc_database']);
		

		if($conn_osc->connect_error){
			$err_message_body[$err_msg_int] = "Error while connecting in <kbd>OsCommerce</kbd>, please see help for more info.";
			$err_msg_int++;
		}else{
			$succ_message_body[$succ_msg_int] = "Success, We are connected to <kbd>OsCommerce</kbd> database! Cheers :D";
			$succ_msg_int++;
		}

		$conn_magento = @new mysqli($_POST['magento_host'], $_POST['magento_user'], $_POST['magento_password'], $_POST['magento_database']);
		if($conn_magento->connect_error){
			$err_message_body[$err_msg_int] = "Error while connecting in <kbd>Magento</kbd>, please see help more info.";
			$err_msg_int++;
		}else{
			$succ_message_body[$succ_msg_int] = "Success, We are connected to <kbd>Magento</kbd> database! Cheers :D";
			$succ_msg_int++;
		}
	}
	
}

?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo TITLE_SITE; ?></title>
	<?php include "includes.php"; ?>
</head>


<body>
<?php include "main_navbar.php"; ?>

<div class="container">
	<div class="page-header">
		<h3>Configuration</h3>
	</div>
	<?php 
		if(!empty($err_message_body) > 0){
			echo "<div class='alert alert-dismissible alert-danger'>
				  <button type='button' class='close' data-dismiss='alert'>×</button>
				  <strong>Oh snap!</strong><br/>";
				  for($i=0; $i<count($err_message_body); $i++){
				  	if(!empty($err_message_body)){
				  		echo "-".$err_message_body[$i]."<br/>";
				  	}				  	
				  }

			echo "</div>";
		}else{
			if ($succ_msg_int >= 2){
		?>
				<script type="text/javascript">
				function countdown() {
				    var i = document.getElementById('counter');
				    if (parseInt(i.innerHTML)<=1) {
				        location.href = 'select-migration.php';
				    }
				    i.innerHTML = parseInt(i.innerHTML)-1;
				}
				setInterval(function(){ countdown(); },500);
				</script>
			<?php
				$_SESSION['osc_host'] = $_POST['osc_host'];
				$_SESSION['osc_user'] = $_POST['osc_user'];
				$_SESSION['osc_password'] = $_POST['osc_password'];
				$_SESSION['osc_database'] = $_POST['osc_database'];
				$_SESSION['magento_host'] = $_POST['magento_host'];
				$_SESSION['magento_user'] = $_POST['magento_user'];
				$_SESSION['magento_password'] = $_POST['magento_password'];
				$_SESSION['magento_database'] = $_POST['magento_database'];
				$succ_message_body[$succ_msg_int] = "Everything here is done after a few seconds you will be redirected to continue the migration.";
				$succ_msg_int++;
				$succ_message_body[$succ_msg_int] = "You will be redirected in <span id='counter'>10</span> second(s).";
				$succ_msg_int++;
				$succ_message_body[$succ_msg_int] = "<kbd>Note: If you want for other migration just click Menu in the navigation bar.</kbd>";
				$succ_msg_int++;
				$_SESSION['all_page_go'] = 1;
				

			}
		}

		if(!empty($succ_message_body) > 0){
			echo "<div class='alert alert-dismissible alert-success'>
				  <button type='button' class='close' data-dismiss='alert'>×</button>
				  <strong>Oryt!</strong><br/>";
				  for($i=0; $i<count($succ_message_body); $i++){
				  	if(!empty($succ_message_body)){
				  		echo "-".$succ_message_body[$i]."<br/>";
				  	}				  	
				  }

			echo "</div>";
		}
	?>
	<div class="row">
		<form name="frm_osc" role="form" method="POST">		
			<div class="col-sm-6">
					<h4><?php echo FLAT_FORM_OSCOMMERCE; ?></h4>				
					<div class="form-group">
					  <label class="control-label" for="focusedInput"><?php echo TITLE_HOST; ?></label>
					  <input class="form-control" id="focusedInput" type="text" name="osc_host" value="<?php if(isset($_POST['osc_host'])){echo this_input($_POST['osc_host']);} ?>">
					</div>

					<div class="form-group">
					  <label class="control-label" for="focusedInput"><?php echo TITLE_USER; ?></label>
					  <input class="form-control" id="focusedInput" type="text" name="osc_user" value="<?php if(isset($_POST['osc_user'])){echo this_input($_POST['osc_user']);} ?>" >
					</div>
				
					<div class="form-group">
					  <label class="control-label" for="focusedInput"><?php echo TITLE_PASSWORD; ?></label>
					  <input class="form-control" id="focusedInput" type="text" name="osc_password" value="<?php if(isset($_POST['osc_password'])){echo this_input($_POST['osc_password']);} ?>" >
					</div>		
				
					<div class="form-group">
					  <label class="control-label" for="focusedInput"><?php echo TITLE_DATABASE; ?></label>
					  <input class="form-control" id="focusedInput" type="text" name="osc_database" value="<?php if(isset($_POST['osc_database'])){echo this_input($_POST['osc_database']);} ?>" >
					</div>
			</div>

			<div class="col-sm-6">
				<h4><?php echo FLAT_FORM_MAGENTO; ?></h4>	
					<div class="form-group">
					  <label class="control-label" for="focusedInput"><?php echo TITLE_HOST; ?></label>
					  <input class="form-control" id="focusedInput" type="text" name="magento_host" value="<?php if(isset($_POST['magento_host'])){echo this_input($_POST['magento_host']);} ?>" >
					</div>

					<div class="form-group">
					  <label class="control-label" for="focusedInput"><?php echo TITLE_USER; ?></label>
					  <input class="form-control" id="focusedInput" type="text" name="magento_user" value="<?php if(isset($_POST['magento_user'])){echo this_input($_POST['magento_user']);} ?>" >
					</div>

					<div class="form-group">
					  <label class="control-label" for="focusedInput"><?php echo TITLE_PASSWORD; ?></label>
					  <input class="form-control" id="focusedInput" type="text" name="magento_password" value="<?php if(isset($_POST['magento_password'])){echo this_input($_POST['magento_password']);} ?>" >
					</div>

					<div class="form-group">
					  <label class="control-label" for="focusedInput"><?php echo TITLE_DATABASE; ?></label>
					  <input class="form-control" id="focusedInput" type="text" name="magento_database" value="<?php if(isset($_POST['magento_database'])){echo this_input($_POST['magento_database']);} ?>" >
					</div>
					<input type="submit" class="btn btn-primary btn-sm pull-right" value="Test Connection" name="submit">
				
			</div>

		</form>
	</div>

	
</div>

<?php include "footer.php"; ?>
</body>
</html>