<?php
include "core.php";
include "includes.php";
?>
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <basefont color="green">
</head>
<style type="text/css">
    BODY { 
        color:#80FF00; 
        background-color:black; } 
</style>
<body bgcolor="black" >

<?php
$conn_osc = mysqli_connect($_SESSION['osc_host'], $_SESSION['osc_user'], $_SESSION['osc_password'], $_SESSION['osc_database']);
if(!$conn_osc){die(mysqli_connect_error());}

$sql_get_osc = "SELECT * from 
                    customers 
                INNER JOIN 
                    address_book 
                on 
                    customers.customers_id = address_book.customers_id 
                INNER JOIN 
                    customers_info 
                on 
                    customers_info.customers_info_id = address_book.customers_id";

$sql_run_get_osc = mysqli_query($conn_osc, $sql_get_osc);
if(mysqli_num_rows($sql_run_get_osc) > 0){
    $array_address = 0;
    $no_conflict = 0;
    while ($row_get_osc = mysqli_fetch_array($sql_run_get_osc)) {
        $_SESSION['osc_id'] = $row_get_osc['customers_id'];
        $_SESSION['osc_gender'] = $row_get_osc['customers_gender'];
        $_SESSION['osc_fname'] = $row_get_osc['customers_firstname'];
        $_SESSION['osc_lname'] = $row_get_osc['customers_lastname'];
        $_SESSION['osc_dob'] = $row_get_osc['customers_dob'];
        $_SESSION['osc_email'] = $row_get_osc['customers_email_address'];
        $_SESSION['osc_address_id'] = $row_get_osc['customers_default_address_id'];
        $_SESSION['osc_telephone'] = $row_get_osc['customers_telephone'];
        $_SESSION['osc_pass_db'] = $row_get_osc['customers_password'];

        $_SESSION['osc_company'] = $row_get_osc['entry_company'];
        $_SESSION['osc_street_address'] = $row_get_osc['entry_street_address'];
        $_SESSION['osc_suburb'] = $row_get_osc['entry_suburb'];
        $_SESSION['osc_postcode'] = $row_get_osc['entry_postcode'];
        $_SESSION['osc_city'] = $row_get_osc['entry_city'];
        $_SESSION['osc_state'] = $row_get_osc['entry_state'];

        $_SESSION['osc_account_created'] = $row_get_osc['customers_info_date_account_created'];
        $_SESSION['osc_last_logon'] = $row_get_osc['customers_info_date_of_last_logon'];
        $_SESSION['osc_number_logon'] = $row_get_osc['customers_info_number_of_logons'];

        $osc_id[$array_address] = $_SESSION['osc_id'];
        $osc_gender[$array_address] = $_SESSION['osc_gender'];
        $osc_fname[$array_address] = $_SESSION['osc_fname'];
        $osc_lname[$array_address] = $_SESSION['osc_lname'];
        $osc_dob[$array_address] = $_SESSION['osc_dob'];
        $osc_email[$array_address] = $_SESSION['osc_email'];
        $osc_address_id[$array_address] = $_SESSION['osc_address_id'];
        $osc_telephone[$array_address] = $_SESSION['osc_telephone'];
        $osc_pass_db[$array_address] = $_SESSION['osc_pass_db'];

        $osc_company[$array_address] = $_SESSION['osc_company'];
        $osc_street_address[$array_address] = $_SESSION['osc_street_address'];
        $osc_suburb[$array_address] = $_SESSION['osc_suburb'];
        $osc_postcode[$array_address] = $_SESSION['osc_postcode'];
        $osc_city[$array_address] = $_SESSION['osc_city'];
        $osc_state[$array_address] = $_SESSION['osc_state'];

        $osc_account_created[$array_address] = $_SESSION['osc_account_created'];
        $osc_last_logon[$array_address] = $_SESSION['osc_last_logon'];
        $osc_number_logon[$array_address] = $_SESSION['osc_number_logon'];
                
        echo "<br/><p>#####################  Customer No. ".$osc_id[$array_address]."  #############################</p>";

        echo "ID: "                    .$osc_id[$array_address]             ."<br/>";
        echo "Gender: "                .$osc_gender[$array_address]         ."<br/>";
        echo "Firstname: "             .$osc_fname[$array_address]          ."<br/>";
        echo "Lastname: "              .$osc_lname[$array_address]          ."<br/>";
        echo "Birthday: "              .$osc_dob[$array_address]            ."<br/>";
        echo "Email: "                 .$osc_email[$array_address]          ."<br/>";
        echo "Address: "               .$osc_address_id[$array_address]     ."<br/>";
        echo "Contact No.: "           .$osc_telephone[$array_address]      ."<br/>";
        echo "Password: "              .$osc_pass_db[$array_address]        ."<br/>";
        echo "Company: "               .$osc_company[$array_address]        ."<br/>";
        echo "Street: "                .$osc_street_address[$array_address] ."<br/>";
        echo "Sub URB: "               .$osc_suburb[$array_address]         ."<br/>";
        echo "Postcode: "              .$osc_postcode[$array_address]       ."<br/>";
        echo "City: "                  .$osc_city[$array_address]           ."<br/>";
        echo "State: "                 .$osc_state[$array_address]          ."<br/>";
        echo "Account Created: "       .$osc_account_created[$array_address]."<br/>";
        echo "Last Login: "            .$osc_last_logon[$array_address]     ."<br/>";
        echo "Number of Login: "       .$osc_number_logon[$array_address]   ."<br/><br/>";
        echo "Last Array Address: "    .$array_address                      ."<br/>";

        $array_address++;
        if($_SESSION['osc_id'] != $array_address){
            echo "Conflict!";
        }else{
            echo "No Conflict";
            $no_conflict++;
        }
    }
    echo "<br/>Arrays with no conflicts: ".$no_conflict;
    $conf = $array_address-$no_conflict;
    echo "<br/>Arrays with conflicts: ".$conf;
}

?>

</body>
</html>