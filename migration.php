<?php
include "core.php";
set_time_limit(7000);
?>
<!DOCTYPE html>
<html>
	<head>
		<title><?php echo TITLE_SITE; ?></title>
		<?php include "includes.php"; ?>
	</head>

<body>
<?php include "main_navbar.php"; ?>
<div class = "container">
	<div class="page-header">
		<h3>Migrating</h3>
	</div>
<div class = "col-sm-6">
	<h3>OsCommerce</h3>
	<div class="col-sm-6">
		<p>Total Accounts: </p>
	</div>
	<div class="col-sm-6">
		<p><?php echo number_format($_SESSION['osc_account_int']); ?></p>
	</div>
</div>

<div class = "col-sm-6">
	<h3>Magento</h3>
	<div class="col-sm-6">
		<p>Account Migrated:</p>
	</div>
	<div class="col-sm-6">
		<p>0</p>
	</div>
</div>
<div class="col-sm-12">
<?php
$conn_osc = mysqli_connect($_SESSION['osc_host'], $_SESSION['osc_user'], $_SESSION['osc_password'], $_SESSION['osc_database']);
if(!$conn_osc){die(mysqli_connect_error());}

$sql_get_osc = "SELECT * from 
                    customers 
                INNER JOIN 
                    address_book 
                on 
                    customers.customers_id = address_book.customers_id 
                INNER JOIN 
                    customers_info 
                on 
                    customers_info.customers_info_id = address_book.customers_id";

$sql_run_get_osc = mysqli_query($conn_osc, $sql_get_osc);
$loaded = 0;
if(mysqli_num_rows($sql_run_get_osc) > 0){
    $array_address = 0;
    $no_conflict = 0;
    while ($row_get_osc = mysqli_fetch_array($sql_run_get_osc)) {
        $osc_id[$array_address] 				= 	$row_get_osc['customers_id'];
        $osc_gender[$array_address] 			= 	$row_get_osc['customers_gender'];
        $osc_fname[$array_address] 				= 	$row_get_osc['customers_firstname'];
        $osc_lname[$array_address] 				= 	$row_get_osc['customers_lastname'];
        $osc_dob[$array_address] 				= 	$row_get_osc['customers_dob'];
        $osc_email[$array_address] 				= 	$row_get_osc['customers_email_address'];
        $osc_address_id[$array_address] 		= 	$row_get_osc['customers_default_address_id'];
        $osc_telephone[$array_address] 			= 	$row_get_osc['customers_telephone'];
        $osc_pass_db[$array_address] 			= 	$row_get_osc['customers_password'];
        $osc_company[$array_address] 			= 	$row_get_osc['entry_company'];
        $osc_street_address[$array_address] 	= 	$row_get_osc['entry_street_address'];
        $osc_suburb[$array_address] 			= 	$row_get_osc['entry_suburb'];
        $osc_postcode[$array_address] 			= 	$row_get_osc['entry_postcode'];
        $osc_city[$array_address] 				= 	$row_get_osc['entry_city'];
        $osc_state[$array_address] 				= 	$row_get_osc['entry_state'];
        $osc_account_created[$array_address]	= 	$row_get_osc['customers_info_date_account_created'];
        $osc_last_logon[$array_address] 		= 	$row_get_osc['customers_info_date_of_last_logon'];
        $osc_number_logon[$array_address] 		= 	$row_get_osc['customers_info_number_of_logons'];
        $array_address++;    
	}
	$loaded = 1;
}
?>
</div>

<?php 
	if($loaded =1){
		echo "<p>All Data has been loaded.. </p>";
		echo "<p>Starting Migration ..</p>";
		echo "<p>Please do not close the browser ..</p>";
		echo "<p>Leave the keyboard ..thankyou :D</p>";
	}else{
		echo "Error .. ".$array_address;
	} 
$conn_magento = mysqli_connect($_SESSION['magento_host'], $_SESSION['magento_user'], $_SESSION['magento_password'], $_SESSION['magento_database']);
echo "<p>Connecting to Magento database.</p>";
if(!$conn_magento){die(mysqli_connect_error());}else{echo "<p class='text-success'>Connected to Magento Database.</p>";}

$array_int = count($osc_id);
$migrate_int = 0;
$accExist = 0;
$save_new_id = "";
$proceed = 0;

for($i = 0; $i < $array_int; $i++){

	// ok reset sa ta sa connection kang peso ra :D
	while($conn_magento->more_results())
	{
	    $conn_magento->next_result();
	    if($res = $conn_magento->store_result()) // added closing bracket
	    {
	        $res->free(); 
	    }
	}
	// End sa reset


	// Step 1 in migrating

	$sql_exist="SELECT email from customer_entity where email ='$osc_email[$i]'";
	$sql_query = mysqli_query($conn_magento,$sql_exist);
	if(mysqli_num_rows($sql_query)>0){
		$accExist++;
		$migrate_int++;
		echo "<p class='text-danger'>Account exist ..</p>";
		echo "<p class='text-danger'>Account No. $i</p><br/>";

	}else{
		$proceed = 1;
		echo "<br/> <p class = 'text-info'>Migrated Accounts:  ". $i."</p>"; 
		$_SESSION['migrated'] = $i;
	}
	if($proceed == 1 ){
		$proceed = 0;

			

		$sql_customer_entity = "INSERT INTO customer_entity (entity_type_id,attribute_set_id,website_id,email,group_id,store_id,is_active,disable_auto_group_change,new_password)VALUES(1,	0,1,'$osc_email[$i]',1,	1,1,0,'$sanitize_pass')";

			if (mysqli_query($conn_magento, $sql_customer_entity)) {
				$proceed = 1;
			    echo "<p class='text-success'>New record created successfully in Customer_entity</p>";
			} else {
				error_msg("Error creating customer_entity");
				echo mysqli_error($conn_magento);
				$proceed = 0;
			}

	}

	// Step 2 in migrating
	if($proceed == 1){
		$proceed = 0;
		$sql_get_id = "SELECT entity_id FROM customer_entity where email = '$osc_email[$i]'";
		$sql_get_id_query = mysqli_query($conn_magento, $sql_get_id);

		if (mysqli_num_rows($sql_get_id_query) > 0) {
		    // output data of each row
		    echo "<p class='text-success'>Success getting ID</p>";
		    while($row_get_id = mysqli_fetch_assoc($sql_get_id_query)) {
		        $save_new_id = $row_get_id['entity_id'];
		        $proceed = 1;
		    }
		} else {
		    error_msg("Error: Getting the NEW ID from customer_entity ");
		    echo mysqli_error($conn_magento);
		}


			

			

			$sql_customer_entity_varchar = "INSERT INTO customer_entity_varchar (
														entity_type_id,
														attribute_id,
														entity_id,
														value
														)VALUES (
														'1',
														'5',
														'$save_new_id',
														'$osc_fname[$i]'
														);";
			$sql_customer_entity_varchar .= "INSERT INTO customer_entity_varchar (
														entity_type_id,
														attribute_id,
														entity_id,
														value
														)VALUES (
														'1',
														'7',
														'$save_new_id',
														'$osc_lname[$i]'
														);";
			$sql_customer_entity_varchar .= "INSERT INTO customer_entity_varchar (
														entity_type_id,
														attribute_id,
														entity_id,
														value
														)VALUES (
														'1',
														'12',
														'$save_new_id',
														'$osc_pass_db[$i]'
														);";
			$sql_customer_entity_varchar .= "INSERT INTO customer_entity_varchar (
														entity_type_id,
														attribute_id,
														entity_id,
														value
														)VALUES (
														'1',
														'3',
														'$save_new_id',
														'English'
														);";

			if (mysqli_multi_query($conn_magento, $sql_customer_entity_varchar)) {
				$proceed = 1;
			    echo "<p class='text-success'>New records created successfully in customer_entity_varchar</p>";
			} else {
				$proceed = 0;
			}

			// ok reset sa ta sa connection kanang peso ra :D
			while($conn_magento->more_results())
			{
			    $conn_magento->next_result();
			    if($res = $conn_magento->store_result()) // added closing bracket
			    {
			        $res->free(); 
			    }
			}
			// End sa reset

			// Step 3 in Migrating
			if($proceed == 1){
				$proceed = 0;
				$sql_save_address = "INSERT INTO customer_address_entity (
																			entity_type_id,
																			parent_id,
																			created_at, 
																			updated_at,
																			is_active
																		) VALUES (
																			'2',
																			'$save_new_id',
																			'$osc_account_created[$i]',
																			'$osc_account_created[$i]',
																			'1'
																			)";
				if(mysqli_query($conn_magento, $sql_save_address)){
					$proceed = 1;
					success_msg("Success creating customer_address_entity");

					$sql_get_address_id = "SELECT entity_id from customer_address_entity where parent_id = '$save_new_id'";
					$sql_get_address_id_query = mysqli_query($conn_magento, $sql_get_address_id);

					if (mysqli_num_rows($sql_get_address_id_query) > 0) {
					    // output data of each row
					    echo "<p class='text-success'>Success getting ID</p>";
					    while($row_get_new_address_id = mysqli_fetch_assoc($sql_get_address_id_query)) {
					        $new_address_id = $row_get_new_address_id['entity_id'];
					        $proceed = 1;
					    }
					    
					} else {
						$proceed = 0;
					    echo error_msg("Error locating ID_ADDRESS");
					}
				}else{
					$proceed = 0;
					error_msg("Error creating customer_address_entity". mysqli_error($conn_magento));
				}
			}



mysqli_query($conn_magento, 'SET foreign_key_checks = 0');



				$sql_customer_entity_int = "INSERT INTO customer_entity_int (
														entity_type_id,
														attribute_id,
														entity_id,
														value
														)VALUES (
														'1',
														'13',
														'$save_new_id',
														'$new_address_id'
														);";
			$sql_customer_entity_int .= "INSERT INTO customer_entity_int (
														entity_type_id,
														attribute_id,
														entity_id,
														value
														)VALUES (
														'1',
														'14',
														'$save_new_id',
														'$new_address_id'
														);";




			if (mysqli_multi_query($conn_magento, $sql_customer_entity_int)) {
				$proceed = 1;
			    echo "<p class='text-success'>New records created successfully in sql_customer_entity_int</p>";
			} else {
				$proceed = 0;
			}

			// ok reset sa ta sa connection kang peso ra :D
			while($conn_magento->more_results())
			{
			    $conn_magento->next_result();
			    if($res = $conn_magento->store_result()) // added closing bracket
			    {
			        $res->free(); 
			    }
			}
			// End sa reset


			/*Step 4 in migration*/
			if($proceed == 1) {
				$proceed = 0;
				$sql_save_address_int = "INSERT INTO customer_address_entity_int(
																				entity_type_id,
																				attribute_id,
																				entity_id,
																				value
																				) 
																				VALUES
																				(
																				'2',
																				'29',
																				'$save_new_id',
																				'0'
																				)";
				if(mysqli_query($conn_magento, $sql_save_address_int)){
					$proceed = 1;
					success_msg("Successfully save customer_address_entity_int");

				}else{
					$proceed = 0;
					error_msg("Error saving customer_address_entity_int");
					echo mysqli_error($conn_magento);

				}
			}


			// ok reset sa ta sa connection kang peso ra :D
			while($conn_magento->more_results())
			{
			    $conn_magento->next_result();
			    if($res = $conn_magento->store_result()) // added closing bracket
			    {
			        $res->free(); 
			    }
			}
			// End sa reset


			/*Step 5 in migrating*/
			if($proceed == 1){
				$proceed = 0;
				$customer_address_entity_text = "INSERT INTO customer_address_entity_text (
																					entity_type_id,
																					attribute_id,
																					entity_id,
																					value
																					)
																					VALUES
																					(
																					'2',
																					'25',
																					'$new_address_id',
																					'$osc_street_address[$i]'
																					)";


				if(mysqli_query($conn_magento, $customer_address_entity_text)){
					$proceed = 1;
					success_msg("Successfully save in customer_address_entity_text");

				}else{
					$proceed = 0;
					error_msg("Error saving in customer_address_entity_text");
					echo mysqli_error($conn_magento);
					echo "<br/> ". $new_address_id;
				}

				// ok reset sa ta sa connection kang peso ra :D
				while($conn_magento->more_results())
				{
				    $conn_magento->next_result();
				    if($res = $conn_magento->store_result()) // added closing bracket
				    {
				        $res->free(); 
				    }
				}
				// End sa reset
				
				/*Step 6 in migration*/
				if($proceed == 1){
					$proceed = 0;
					$sql_customer_address_entity_varchar = "INSERT INTO customer_address_entity_varchar (
																entity_type_id,
																attribute_id,
																entity_id,
																value
																)VALUES (
																'2',
																'20',
																'$new_address_id',
																'$osc_fname[$i]'
																);";
					$sql_customer_address_entity_varchar .= "INSERT INTO customer_address_entity_varchar (
																entity_type_id,
																attribute_id,
																entity_id,
																value
																)VALUES (
																'2',
																'22',
																'$new_address_id',
																'$osc_lname[$i]'
																);";
					$sql_customer_address_entity_varchar .= "INSERT INTO customer_address_entity_varchar (
																entity_type_id,
																attribute_id,
																entity_id,
																value
																)VALUES (
																'2',
																'24',
																'$new_address_id',
																'$osc_company[$i]'
																);";
					$sql_customer_address_entity_varchar .= "INSERT INTO customer_address_entity_varchar (
																entity_type_id,
																attribute_id,
																entity_id,
																value
																)VALUES (
																'2',
																'26',
																'$new_address_id',
																'$osc_city[$i]'
																);";
					$sql_customer_address_entity_varchar .= "INSERT INTO customer_address_entity_varchar (
																entity_type_id,
																attribute_id,
																entity_id,
																value
																)VALUES (
																'2',
																'27',
																'$new_address_id',
																'SG'
																);";
					$sql_customer_address_entity_varchar .= "INSERT INTO customer_address_entity_varchar (
																entity_type_id,
																attribute_id,
																entity_id,
																value
																)VALUES (
																'2',
																'28',
																'$new_address_id',
																'$osc_state[$i]'
																);";
					$sql_customer_address_entity_varchar .= "INSERT INTO customer_address_entity_varchar (
																entity_type_id,
																attribute_id,
																entity_id,
																value
																)VALUES (
																'2',
																'30',
																'$new_address_id',
																'$osc_postcode[$i]'
																);";
					$sql_customer_address_entity_varchar .= "INSERT INTO customer_address_entity_varchar (
																entity_type_id,
																attribute_id,
																entity_id,
																value
																)VALUES (
																'2',
																'31',
																'$new_address_id',
																'$osc_telephone[$i]'
																);";

					if (mysqli_multi_query($conn_magento, $sql_customer_address_entity_varchar)) {
						$proceed = 1;
					    echo "<p class='text-success'>New records created successfully in customer_address_entity_varchar</p>";
					    success_msg("Migrating Next Account --------------------------------------------------------------");
					} else {
						$proceed = 0;
					    echo "Error: " . $sql . "<br>" . mysqli_error($conn_magento);
					}
						}

mysqli_query($conn_magento, 'SET foreign_key_checks = 1');

			}
			
		}else{
			success_msg("Proceeding to next account----------------------------------------------------------------");
			$proceed = 0;
		}

	

}

?>
</div> <!-- Container Closing -->

	

</body>
</html>