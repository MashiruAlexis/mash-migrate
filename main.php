<?php
include "core.php";
if(!isset($_SESSION['osc_host']) or !isset($_SESSION['magento_host'])){
	header("location:" . SITE_BASE_URL . "reset.php");
}
$_SESSION['osc_host']="localhost";
$mysqli_osc = new mysqli($_SESSION['osc_host'], $_SESSION['osc_user'], $_SESSION['osc_password'], $_SESSION['osc_database']);


if (mysqli_connect_errno()) {
    $msg_state_osc = "<span class='text-error'>Error, please go back or see help section.</span>";
}else{
	$msg_state_osc =  "<span class='text-success'>Connected</span>";
}

if ($result_osc = $mysqli_osc->query("SELECT customers_id FROM customers ORDER BY customers_id")) {

    $row_cnt = $result_osc->num_rows;

    $accout_int_osc = $row_cnt;

    $_SESSION['osc_account_int'] = $accout_int_osc;

    $result_osc->close();
}

$mysqli_osc->close();

$mysqli_magento = new mysqli($_SESSION['magento_host'], $_SESSION['magento_user'], $_SESSION['magento_password'], $_SESSION['magento_database']);

if (mysqli_connect_errno()) {
    $msg_state = "<span class='text-error'>Error, please go back or see help section.</span>";
}else{
	$msg_state =  "<span class='text-success'>Connected</span>";
}

if ($result = $mysqli_magento->query("SELECT entity_id FROM customer_entity ORDER BY entity_id")) {

    $row_cnt = $result->num_rows;

    $accout_int = $row_cnt;

    $result->close();
}


$mysqli_magento->close();
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo TITLE_SITE; ?></title>
	<?php include "includes.php"; ?>
</head>
<body>
<?php include "main_navbar.php"; ?>

<div class="container">
	<div class="page-header">
		<h3>Overview</h3>
	</div>

	<div class="row">
		<div class = "col-sm-6">
			<h3><?php echo FLAT_FORM_OSCOMMERCE; ?></h3>
			<div class="col-sm-3">
				<p>Status: </p>
				<!-- <p>Account(s): </p> -->
				<p>Host: </p>
				<p>User: </p>
				<p>Password: </p>
				<p>Database: </p>
			</div>
			<div class="col-sm-3">
				<p><?php echo $msg_state_osc; ?></p>
				<!-- <p><?php echo number_format($accout_int_osc); ?></p> -->
				<p><?php echo $_SESSION['osc_host']; ?></p>
				<p><?php echo $_SESSION['osc_user']; ?></p>
				<p><?php if(empty($_SESSION['osc_password'])){echo "********";} ?></p>
				<p><?php echo $_SESSION['osc_database']; ?></p>
			</div>
		</div>

		<div class = "col-sm-6">
			<h3><?php echo FLAT_FORM_MAGENTO; ?></h3>
			<div class="col-sm-3">
				<p>Status: </p>
				<p>Account(s): </p>
				<p>Host: </p>
				<p>User: </p>
				<p>Password: </p>
				<p>Database: </p>
			</div>
			<div class="col-sm-3">
				<p><?php echo $msg_state; ?></p>
				<p><?php echo number_format($accout_int); ?></p>
				<p><?php echo $_SESSION['magento_host']; ?></p>
				<p><?php echo $_SESSION['magento_user']; ?></p>
				<p><?php if(empty($_SESSION['magento_password'])){echo "********";} ?></p>
				<p><?php echo $_SESSION['magento_database']; ?></p>
				
			</div>
		</div>

		<form name="frm_migrate" method="POST" action="migration.php">
			<input type="submit" value="Star Migration" class="btn btn-primary pull-right">
			<a href="reset.php" class="btn btn-primary ">Reset</a>
		</form>
	</div>

</div>
<?php include "footer.php"; ?>
</body>
</html>