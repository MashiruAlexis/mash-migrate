<?php
include "core.php";
$conn_osc = mysqli_connect($_SESSION['osc_host'], $_SESSION['osc_user'], $_SESSION['osc_password'], $_SESSION['osc_database']);
if(!$conn_osc){echo "Error Connecting to database.". mysqli_error($conn_osc);}

$sql_get_products = "SELECT products_status from products";
$sql_run_products = mysqli_query($conn_osc, $sql_get_products);
$sku_inc = 0;
$sku_dec = 0;
if(mysqli_num_rows($sql_run_products) > 0){
	while($row_get_products = mysqli_fetch_array($sql_run_products)){
		if($row_get_products['products_status'] == '1'){
			$sku_inc++;
		}else{
			$sku_dec++;
		}		
	}
}
?>
<!DOCTYPE html>
<html>
	<head>
		<title><?php echo TITLE_SITE; ?></title>
		<?php include "includes.php"; ?>
	</head>
<body>
<?php include "main_navbar.php"; ?>
<div class = "container">
	<div class="page-header">
		<h3>Migration</h3>
	</div>
<div class = "col-sm-6">
	<h3>OsCommerce</h3>
	<div class="col-sm-12">
		<p>Products: </p>
	</div>
	<div class="col-sm-6">
		<p><label class="btn-success">Active</label></p>
	</div>
	<div class="col-sm-6">
		<p><?php echo $sku_inc; ?></p>
	</div>
	<div class="col-sm-8">
		<p><label class="btn-warning">Deactivated</label></p>
	</div>
	<div class="col-sm-4">
		<p><?php echo $sku_dec; ?></p>
	</div>
</div>
<div class="col-sm-12">
<h3></h3>
<div class="container">
	<a href="migrating-url.php"><button type="button" class="btn btn-success">Start Generating <kbd>.htaccess</kbd> file.</button></a>
</div>
</div> <!-- Container Closing -->
</body>
</html>