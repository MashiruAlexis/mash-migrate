<?php
include "core.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title>Status</title>
	<?php include "includes.php" ?>
	<meta http-equiv="refresh" content="3">
</head>
<body>
<div class = "container">
	 <table class="table">
    <thead>
      <tr>
        <th>Modules</th>
        <th>Count</th>
      </tr>
    </thead>
    <tbody>
      <tr class="success">
        <td>Migrated: </td>
        <td><?php get_success_migrate($_SESSION['success_migrate']); ?></td>
      </tr>
      <tr class="danger">
        <td>Duplicate(s):</td>
        <td><?php get_duplicate($_SESSION['duplicate_account']); ?></td>
      </tr>
      <tr class="info">
        <td>Progress: </td>
        <td><?php
        		// $_SESSION['osc_account_int'])
        		$percent = (($_SESSION['success_migrate']+$_SESSION['duplicate_account'])/ 100) ;
        		$percent = $percent * 100;
        		echo number_format($percent, 2)."%";
        	?></td>
      </tr>
    </tbody>
  </table>
<h2><?php echo $_SESSION['migrated']; ?></h2>
</div>
</body>
</html>