<script type="text/javascript">
// Popup window code
function newPopup(url) {
  popupWindow = window.open(
    url,'popUpWindow','height=300,width=400,left=10,top=10,resizable=yes,scrollbars=no,toolbar=no,menubar=no,location=no,directories=no,status=yes')
}
</script>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="../index.php">Mash Migrate</a>
    </div>
    <div>
      <ul class="nav navbar-nav">
        <li><a href="#" data-toggle="modal" data-target="#help_modal">Help</a></li> 
        <li><a href="#" data-toggle="modal" data-target="#myModal">About Us</a></li> 
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">MENU <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo SITE_BASE_URL; ?>main.php">Customers Migration</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="<?php echo SITE_BASE_URL; ?>orders.php">Orsers Migration</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="<?php echo SITE_BASE_URL; ?>urlmigrate.php">Url Migration</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="<?php echo SITE_BASE_URL; ?>msrp-view.php">MSRP Migration</a></li>  
            <li role="separator" class="divider"></li>
            <li><a href="<?php echo SITE_BASE_URL; ?>category-view.php">Category Sale Migration</a></li>  
            <li role="separator" class="divider"></li>
            <li><a href="<?php echo SITE_BASE_URL; ?>update.php">Fix Databse</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="<?php echo SITE_BASE_URL; ?>clearancecat-view.php">Category Clearance Categorization</a></li>                      
          </ul>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="JavaScript:newPopup('status.php');">Open a popup window</a></li>
        <li><a href="<?php echo "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>">Refresh</a></li>
        <li><a href="<?php echo SITE_BASE_URL; ?>reset.php">RESET</a></li>
      </ul>
    </div>
  </div>
</nav>


<!-- Modal About Us -->
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Mashiru's Way</h4>
      </div>
      <div class="modal-body">
        <center>
        <p>If debugging is the process of removing bugs, then programming must be the process of putting them in.</p>
      </center> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Alright</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Rock n Roll</button>
      </div>
    </div>
  </div>
</div>

<!-- Modals Help -->
<div class="modal" id="help_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">HELP</h4>
      </div>
      <div class="modal-body">
        <center>
        <p><h2>Call <kbd>911</kbd></h2></p>
      </center> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Alright</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Rock n Roll</button>
      </div>
    </div>
  </div>
</div>