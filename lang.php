<?php


define("TITLE_SITE", 				"MASH MIGRATE");
define("SITE_BRAND_NAME", 			"Migrate");
define("TITLE_HOST", 				"Host");
define("TITLE_USER", 				"User");
define("TITLE_PASSWORD", 			"Password");
define("TITLE_DATABASE", 			"Database");
define("FLAT_FORM_OSCOMMERCE", 		"OsCommerce");
define("FLAT_FORM_MAGENTO", 		"Magento");
define("SUCCESS_TESTCONNECT_MSG", 	"Success, The connection parameter you provided is good to go!");
define("ORDERS_TITLE", 				"Orders Migration");
define("MSRP_MIGRATION", "MSRP Migration");
define("CLEARANCE_CATEGORIZATION", "Clearance Caategorization");
define("MIGRATIONS", "Migrations");

// define("SITE_BASE_URL", "http://www.new.lioncityco.com/migrate/");
define("SITE_BASE_URL", "http://192.168.2.99/html/sites/mash-migrate.com/");
#define("SITE_BASE_URL", "");
define("MAGE_ADDRESS", "../dev-new.lioncityco/app/Mage.php");
define("ORDER_FINAL_ADDRESS","order-final.php");
?>