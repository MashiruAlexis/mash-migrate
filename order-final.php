<?php
ini_set('memory_limit', '-1');
set_time_limit(6000);
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo ORDERS_TITLE; ?></title>
    <?php include "includes.php"; ?>
</head>
<body>

<?php include "main_navbar.php"; ?>

<?php
include "includes.php";
include "core.php";
echo "Please wait while finalizing orders ...<br/>";
require_once (MAGE_ADDRESS);
ini_set("error_reporting",E_ALL);
ini_set("display_errors",true);
umask(0);
Mage::app('admin');

try
{
$orders = Mage::getModel('sales/order')->getCollection();
//->addFieldToFilter('created_at', array('from' => '2000-09-01', 'to' => '2015-09-02'));
//->addAttributeToFilter('status', array('neq' => Mage_Sales_Model_Order::STATE_COMPLETE));
foreach($orders as $order)
{
//grab orders from above (all that are not complete from the month of September), and set them to Complete status programmatically.
$order->setData('state', "closed");
$order->setStatus("closed");
//$history = $order->addStatusHistoryComment('Order was set to Complete by our automation tool.', false);
//$history->setIsCustomerNotified(false);
$order->save();
}
}
catch(Exception $e)
{
echo $e->getMessage();
}

?>
<script>
  $(document).ready(function(){
    alert('Done! Migrating');
  });
</script>
</body>
</html>
