<?php
//include "core.php";
include "includes.php";
set_time_limit(6000);
?>

<!DOCTYPE html>
<html>
<head>
	<title><?php echo MSRP_MIGRATION; ?></title>
</head>
<style type="text/css">
	
	button#load{
    padding: 0 3em;
    outline: none;
    border: none;
    color: #fff;
    text-transform: uppercase;
    font-weight: 700;
    letter-spacing: 1px;
    font-size: 1em;
    line-height: 4;
    overflow: hidden;
    border-radius: 5px;
    background: rgba(0,0,0,0.2);
    text-align: center;
    cursor: pointer;
    margin: 20px auto;
    display: block;
}
</style>
<script>
$(document).ready(function(){
    $(document).ajaxStart(function(){
        $("#wait").css("display", "block");
    });
    $(document).ajaxComplete(function(){
        $("#wait").css("display", "none");
    });
    $("#load").click(function(){
        $("#txt").load("msrp-run.php");
    });
});
</script>
<body>
<?php include "main_navbar.php"; ?>



<button id="load">Star RCP Migration</button>
<button id="stop" onclick="xhr.abort()">Stop RCP Migration</button>
<center><div id="wait" style="display:none;"><img src='img/preloader.gif'/><br>Migrating RCP</div></center>
<center><div id="txt">
                        <h2>
                        <?php
						  if(!$_SESSION['all_page_go'] == 1 or !isset($_SESSION['all_page_go'])){
						  	error_msg("State: Disconnected");
						  	exit();
						  }else{			
						  	info_msg("State: Ready");
						  }
						?>
                        </h2></div></center>
</body>
</html>
