<?php
function err_msg_type($type, $msg, $title){
	echo "<div class='alert alert-dismissible alert-".$type."'>
		  <button type='button' class='close' data-dismiss='alert'>×</button>
		  <strong>".$title."</strong> <br/>".$msg."</div>";
}

function session_all_reset(){
	session_unset();  
}

function success_msg($success_msg_body){
	echo "<p class='text-success'>".$success_msg_body."<br/></p>";
}
function error_msg($err_msg_body){
	echo "<p class='text-danger'>".$err_msg_body."<br/></p>";
}

function info_msg($info_msg_body){
	echo "<p class='text-info'>".$info_msg_body."<br/></p>";
}

function disp_msg($a){
	echo "<p>".$a."<br/></p>";
}

function get_success_migrate($a){
	if(empty($a)){
		echo "0";
		$_SESSION['success_migrate'] = 0;
	}else{
		echo $_SESSION['success_migrate'];
	}
}

function get_duplicate($b){
	if(empty($b)){
		echo "0";
		$_SESSION['duplicate_account'] = 0;
	}else{
		echo $_SESSION['duplicate_account'];
	}
}
function _recreateTime($a){
		$osc_oldDate_comment_format = $a;
		$convert_osc_oldDate_comment_format = strtotime($osc_oldDate_comment_format);
		$magento_formated_date = date("Y-m-d h:i:sA", $convert_osc_oldDate_comment_format);
		return $magento_formated_date;
	}
function _validateConnection(){
	if(!$_SESSION['all_page_go'] == 1 or !isset($_SESSION['all_page_go'])){
		error_msg("Error, Connect to database first.");
		exit();
	}
}



?>