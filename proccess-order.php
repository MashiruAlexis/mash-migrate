<?php
include "core.php";
if(!$_SESSION['all_page_go'] == 1 or !isset($_SESSION['all_page_go'])){
	error_msg("Error, add connection string and kaboomm!");
	exit();
}

require_once MAGE_ADDRESS;
umask(0);
Mage::app();


echo "Website ID: " . Mage::app()->getWebsite()->getId() . "<br/>"; 
echo "Website Name: " . Mage::app()->getWebsite()->getName() . "<br/>"; 
echo "Store ID: " . Mage::app()->getStore()->getId() . "<br/>"; 
echo "Store Name: ".Mage::app()->getStore()->getName(). "<br/>";
echo "Store code: ". Mage::app()->getStore()->getCode()."<br/>";


$conn_oscommerce  = mysqli_connect($_SESSION['osc_host'], $_SESSION['osc_user'], $_SESSION['osc_password'], $_SESSION['osc_database']);
if(!$conn_oscommerce){
	error_msg("Error connecting to OsCommerce Database.");
}

$sql_connect_osc_database = "SELECT * from orders							";
$sql_run_connect_osc_database = mysqli_query($conn_oscommerce, $sql_connect_osc_database);
$orders_address = 0;
if(mysqli_num_rows($sql_run_connect_osc_database)>0){
	while($row_run_connect_database = mysqli_fetch_array($sql_run_connect_osc_database)){
		//orders
		$osc_orders_id[$orders_address]						= $row_run_connect_database['orders_id'];
		$osc_customers_id[$orders_address] 					= $row_run_connect_database['customers_id'];
		$osc_customers_name[$orders_address] 				= $row_run_connect_database['customers_name'];
		$osc_customers_company[$orders_address] 			= $row_run_connect_database['customers_company'];
		$osc_customers_street_address[$orders_address] 		= $row_run_connect_database['customers_street_address'];
		$osc_customers_suburb[$orders_address] 				= $row_run_connect_database['customers_suburb'];
		$osc_customers_city[$orders_address] 				= $row_run_connect_database['customers_city'];
		$osc_customers_postcode[$orders_address] 			= $row_run_connect_database['customers_postcode'];
		$osc_customers_state[$orders_address]				= $row_run_connect_database['customers_state'];
		$osc_customers_country[$orders_address] 			= $row_run_connect_database['customers_country'];
		$osc_customers_telephone[$orders_address] 			= $row_run_connect_database['customers_telephone'];
		$osc_customers_email_address[$orders_address] 		= $row_run_connect_database['customers_email_address'];
		$osc_customers_address_format_id[$orders_address] 	= $row_run_connect_database['customers_address_format_id'];
		$osc_delivery_name[$orders_address] 				= $row_run_connect_database['delivery_name'];
		$osc_delivery_company[$orders_address] 				= $row_run_connect_database['delivery_company'];
		$osc_delivery_street_address[$orders_address] 		= $row_run_connect_database['delivery_street_address'];
		$osc_delivery_suburb[$orders_address] 				= $row_run_connect_database['delivery_suburb'];
		$osc_delivery_city[$orders_address] 				= $row_run_connect_database['delivery_city'];
		$osc_delivery_postcode[$orders_address] 			= $row_run_connect_database['delivery_postcode'];
		$osc_delivery_state[$orders_address] 				= $row_run_connect_database['delivery_state'];
		$osc_delivery_country[$orders_address] 				= $row_run_connect_database['delivery_country'];
		$osc_delivery_address_format_id[$orders_address] 	= $row_run_connect_database['delivery_address_format_id'];
		$osc_billing_name[$orders_address] 					= $row_run_connect_database['billing_name'];
		$osc_billing_company[$orders_address] 				= $row_run_connect_database['billing_company'];
		$osc_billing_street_address[$orders_address] 		= $row_run_connect_database['billing_street_address'];
		$osc_billing_suburb[$orders_address] 				= $row_run_connect_database['billing_suburb'];
		$osc_billing_city[$orders_address] 					= $row_run_connect_database['billing_city'];
		$osc_billing_postcode[$orders_address] 				= $row_run_connect_database['billing_postcode'];
		$osc_billing_state[$orders_address] 				= $row_run_connect_database['billing_state'];
		$osc_billing_country[$orders_address] 				= $row_run_connect_database['billing_country'];
		$osc_billing_address_format_id[$orders_address] 	= $row_run_connect_database['billing_address_format_id'];
		$osc_payment_method[$orders_address] 				= $row_run_connect_database['payment_method'];
		$osc_cc_type[$orders_address] 						= $row_run_connect_database['cc_type'];
		$osc_cc_owner[$orders_address] 						= $row_run_connect_database['cc_owner'];
		$osc_cc_number[$orders_address] 					= $row_run_connect_database['cc_number'];
		$osc_cc_expires[$orders_address] 					= $row_run_connect_database['cc_expires'];
		$osc_last_modified[$orders_address]					= $row_run_connect_database['last_modified'];
		$osc_date_purchased[$orders_address]				= $row_run_connect_database['date_purchased'];
		$osc_orders_status[$orders_address] 				= $row_run_connect_database['orders_status'];
		$osc_orders_date_finish[$orders_address]			= $row_run_connect_database['orders_date_finish'];
		$osc_currency[$orders_address]						= $row_run_connect_database['currency'];
		$osc_currency_value[$orders_address]				= $row_run_connect_database['currency_value'];
		$orders_address++;
	}
}
mysqli_close($conn_oscommerce);
// himu ta ug settings first para to simplify everything
$migrate_store_id  = Mage::app()->getStore()->getId();
$order_length = count($osc_orders_id);
$config  = Mage::getConfig()->getResourceConnectionConfig("default_setup");
$dbinfo = array("host" => $config->host,
	            "user" => $config->username,
	            "pass" => $config->password,
	            "dbname" => $config->dbname
);
$magento_hostname 	= $dbinfo["host"];
$magento_user 		= $dbinfo["user"];
$magento_password 	= $dbinfo["pass"];
$magento_dbname 	= $dbinfo["dbname"];
$conn_magento = mysqli_connect($magento_hostname, $magento_user, $magento_password, $magento_dbname);
if(!conn_magento){
	error_msg("Error Connecting to magento database, please review your code and recompile it again.");
}else{
	success_msg("Success, we are now connected to magento database.");
}
for($i = 0; $i < $order_length; $i++){
	echo "========================= Order Migration No. $i ==============================<br/><br/>";
	$sql_get_customer_id = "SELECT entity_id from customer_entity where email = '$osc_customers_email_address[$i]'";
	$sql_run_customer_id = mysqli_query($conn_magento, $sql_get_customer_id);
		if(mysqli_num_rows($sql_run_customer_id)>0){
		while($row_get_customer_id = mysqli_fetch_array($sql_run_customer_id)){
			$new_customer_id = $row_get_customer_id['entity_id'];
			success_msg("New ID found, proceeding migration ...");
		}
		$id=$new_customer_id; // get Customer Id
		error_msg("Customer information found in magento database ...");
	}else{
		$id=1; // get Customer Id
		info_msg("The customer of this order is not in the magento database.");
		info_msg($osc_customers_email_address[$i]);
		$_SESSION['stop_order'] = 1;
	}
	info_msg("Customer Name: $osc_customers_name[$i]");	
	$customer = Mage::getModel('customer/customer')->load($id);	 
	$transaction = Mage::getModel('core/resource_transaction');
	$storeId = $customer->getStoreId();
	$reservedOrderId = Mage::getSingleton('eav/config')->getEntityType('order');
	echo $reservedOrderId." - Reserve order here.";	 
	$order = Mage::getModel('sales/order')
	->setOrderId 			($osc_orders_id[$i])
	->setStoreId 				($migrate_store_id)
	->setQuoteId 				(0)
	->setGlobal_currency_code 	($osc_currency[$i])
	->setBase_currency_code 	($osc_currency[$i])
	->setStore_currency_code 	($osc_currency[$i])
	->setOrder_currency_code 	($osc_currency[$i]);
	success_msg("Store currency is SET ...");
	//Set your store currency USD or any other
	 
	// set Customer data
	$order->setCustomer_email($customer->getEmail())
	->setCustomerFirstname($customer->getFirstname())
	->setCustomerLastname($customer->getLastname())
	->setCustomerGroupId($customer->getGroupId())
	->setCustomer_is_guest(0)
	->setCustomer($customer);
	success_msg("Customer data is SET ...");
	 
	// set Billing Address
	$billing =  Mage::getModel('customer/address')->load($customer->default_billing);
	$billingAddress = Mage::getModel('sales/order_address')
	->setStoreId($storeId)
	->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_BILLING)
	->setCustomerId($customer->getId())
	->setCustomerAddressId($customer->getDefaultBilling())
	->setCustomer_address_id($billing->getEntityId())
	->setPrefix($billing->getPrefix())
	->setFirstname($billing->getFirstname())
	->setMiddlename($billing->getMiddlename())
	->setLastname($billing->getLastname())
	->setSuffix($billing->getSuffix())
	->setCompany($billing->getCompany())
	->setStreet($billing->getStreet())
	->setCity($billing->getCity())
	->setCountry_id($billing->getCountryId())
	->setRegion($billing->getRegion())
	->setRegion_id($billing->getRegionId())
	->setPostcode($billing->getPostcode())
	->setTelephone($billing->getTelephone())
	->setFax($billing->getFax());
	if($order->setBillingAddress($billingAddress)){
		success_msg("Billing address is SET ...");
	}

	 
	$shipping = Mage::getModel('customer/address')->load($customer->default_shipping);
	$shippingAddress = Mage::getModel('sales/order_address')
	->setStoreId 			($storeId)
	->setAddressType 		(Mage_Sales_Model_Quote_Address::TYPE_SHIPPING)
	->setCustomerId 		($customer->getId())
	->setCustomerAddressId 	($customer->getDefaultShipping())
	->setCustomer_address_id($shipping->getEntityId())
	->setPrefix 			($shipping->getPrefix())
	->setFirstname 			($shipping->getFirstname())
	->setMiddlename 		($shipping->getMiddlename())
	->setLastname 			($shipping->getLastname())
	->setSuffix 			($shipping->getSuffix())
	->setCompany 			($shipping->getCompany())
	->setStreet 			($shipping->getStreet())
	->setCity 				($shipping->getCity())
	->setCountry_id 		($shipping->getCountryId())
	->setRegion 			($shipping->getRegion())
	->setRegion_id 			($shipping->getRegionId())
	->setPostcode 			($shipping->getPostcode())
	->setTelephone	 		($shipping->getTelephone())
	->setFax 				($shipping->getFax());
	success_msg("Shipping data is SET ...");
	 
	

	/*->setShippingDescription($this->getCarrierName('flatrate'));*/
	/*some error i am getting here need to solve further*/
	 
	//you can set your payment method name here as per your need
	$orderPayment = Mage::getModel('sales/order_payment')
	->setStoreId($storeId)
	->setCustomerPaymentId(0)
	->setMethod("purchaseorder")
	->setPo_number($osc_orders_id[$i]);
	$order->setPayment($orderPayment);
	success_msg("Payment method is SET ...");
	 
	// let say, we have 2 products
	//check that your products exists
	//need to add code for configurable products if any
	
	//$products = array('1' => array('qty' => 2));
	 $new_conn1 = mysqli_connect($_SESSION['osc_host'], $_SESSION['osc_user'], $_SESSION['osc_password'], $_SESSION['osc_database']);
	 	$sql_get_flatrate = "SELECT value from orders_total where orders_id = '$osc_orders_id[$i]' and sort_order = '2'";
		$sql_run_flatrate = mysqli_query($new_conn1, $sql_get_flatrate);
		if(mysqli_num_rows($sql_run_flatrate)>0){
			info_msg("Shipping rate SET ...");
			while($row_get_flatrate = mysqli_fetch_array($sql_run_flatrate)){
				$osc_flatrate = $row_get_flatrate['value'];
			}
			$order->setShippingAddress($shippingAddress)
			->setShipping_method 		('flatrate_flatrate')
			->setShippingAmount 		($osc_flatrate)
    		->setBaseShippingAmount 	($osc_flatrate);
		}else{
			info_msg("No Shipping rate for this order ...");
		}
		$sql_get_fltax = "SELECT value from orders_total where orders_id = '$osc_orders_id[$i]' and sort_order = '3'";
		$sql_run_fltax = mysqli_query($new_conn1, $sql_get_fltax);
		if(mysqli_num_rows($sql_run_fltax)>0){
			info_msg("TAX here ...");
			while($row_get_fltax = mysqli_fetch_array($sql_run_fltax)){
				$osc_fltax	 = $row_get_fltax['value'];
			}
		}
	//$_product = Mage::getModel('catalog/product')->load($productId);
	$index_order_item = 0;
	$rowTotal = 0;
	if(!$new_conn1){
		error_msg("Error in getting product information.");
	}else{
		info_msg("This is the order ID $osc_orders_id[$i]");
		if(!$osc_orders_id[$i]){
			info_msg("No ID found!");
			info_msg("No product found in this ORDER");
			$_SESSION['stop_order'] = 1;

		}else{
			$sql_get_products = "SELECT * from orders_products where orders_id = '$osc_orders_id[$i]'";
			$sql_run_get_products = mysqli_query($new_conn1, $sql_get_products);
			if(mysqli_num_rows($sql_run_get_products)>0){
				info_msg("Found product details ...");

				while($row_get_products = mysqli_fetch_array($sql_run_get_products)){
					$get_tax_percent = (($osc_fltax/($row_get_products['products_quantity']*$row_get_products['final_price']))*100);
					$get_tax_percent_final =  round((float)$get_tax_percent, 2);
					$orderItem[$index_order_item] = Mage::getModel('sales/order_item')
					->setStoreId 				($storeId)
					->setQuoteItemId 			(0)
					->setQuoteParentItemId 		(NULL)
					->setProductId 				($row_get_products['orders_products_id'])
					->setProductType 			(1)
					->setQtyBackordered 		(NULL)
					->setTotalQtyOrdered 		($row_get_products['products_quantity'])
					->setQtyOrdered 			($row_get_products['products_quantity'])
					->setName 					($row_get_products['products_name'])
					->setSku 					($row_get_products['products_model'])
					->setPrice 					($row_get_products['products_price'])
					->setBasePrice 				($row_get_products['final_price'])
					->setOriginalPrice 			($row_get_products['products_price'])
					->setTaxPercent 			($get_tax_percent_final)
					->setTaxAmount 				($osc_fltax)
					->setRowTotal 				($row_get_products['products_quantity']*$row_get_products['final_price'])
					->setBaseRowTotal 			($row_get_products['products_quantity']*$row_get_products['final_price']);
					$rowTotal = $row_get_products['products_quantity']*$row_get_products['final_price'];
					echo $row_get_products['orders_products_id'];
					success_msg("Success getting product No. $index_order_item");
					$index_order_item++;

				}
			}else{
				error_msg("Product details not found!");
			}
		}
	}

	// add sa comment
	// so let's recreate the date format from oscommerce with mashiru magic!
	$sql_get_comments = "SELECT * from orders_status_history where orders_id = '$osc_orders_id[$i]'";
	$sql_run_get_comments = mysqli_query($new_conn1, $sql_get_comments);
	if(mysqli_num_rows($sql_run_get_comments)>0){
		success_msg("Comment found and SET ...");
		while($row_get_comments = mysqli_fetch_array($sql_run_get_comments)){
			if($row_get_comments['customer_notified'] == 1){
				$history = Mage::getModel('sales/order_status_history')
			    ->setStatus 				($order->getStatus())
			    ->setComment 				($row_get_comments['comments'])
			    ->setEntityName 			(Mage_Sales_Model_Order::HISTORY_ENTITY_NAME)
			    ->setIsCustomerNotified 	(true)
			    ->setCreatedAt 		 		(_recreateTime($row_get_comments['date_added']));
				$order->addStatusHistory 	($history);
			}else{
				$history = Mage::getModel('sales/order_status_history')
			    ->setStatus 				($order->getStatus())
			    ->setComment 				($row_get_comments['comments'])
			    ->setEntityName 			(Mage_Sales_Model_Order::HISTORY_ENTITY_NAME)
			    ->setIsCustomerNotified 	(false)
			    ->setCreatedAt 				(_recreateTime($row_get_comments['date_added']));
				$order->addStatusHistory 	($history);
			}
		}
	}else{
		info_msg("This order does not contain any comment(s) ...");
	}
	// Add comment
	$history = Mage::getModel('sales/order_status_history')
	    ->setStatus 				($order->getStatus())
	    ->setComment 				("This order was proccessed from the OLD LIONCITY SITE.")
	    ->setEntityName 			(Mage_Sales_Model_Order::HISTORY_ENTITY_NAME)
	    ->setIsCustomerNotified 	(false)
	    ->setCreatedAt 				(Mage::getModel('core/date')->date('Y-m-d H:i:s'));
		$order->addStatusHistory 	($history);

	 for($z = 0; $z <= count($index_order_item); $z++){
		$order->addItem($orderItem[$z]);		
	 }
	$subTotal = $rowTotal+$osc_fltax;
	$order->setSubtotal($subTotal)
			->setBaseSubtotal($subTotal)
			->setGrandTotal($rowTotal+$osc_fltax+$osc_flatrate)
			->setBaseGrandTotal($rowTotal+$osc_fltax+$osc_flatrate);
	success_msg("Sub total and Base total is SET ...");
	$transaction->addObject($order);
	$transaction->addCommitCallback(array($order, 'place'));
	$transaction->addCommitCallback(array($order, 'save'));
	if($_SESSION['stop_order'] == 0){
		$transaction->save();
		
		success_msg("Success Migrating Order No. $i");
		$_SESSION['stop_order'] = 0;
	}else{
		error_msg("Order Migration was stop for this entity, procceding ...");
		$_SESSION['stop_order'] = 0;
	}
}


if($i === $order_length){
	echo "<br/>################# NOTICE #################<br/>";
	success_msg("Order Migration Complete");
	info_msg("proccessed: $order_length / $i");
}
?>
<html>
	<head>
    	<title>Finalizing</title>
    </head>
<body>
<h2><a href="<?php echo ORDER_FINAL_ADDRESS; ?>">Click here</a> to next stage of order migration.</h2>
</body>