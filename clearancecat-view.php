<?php
/**
 *	Clearance Categorization
 */
include "core.php";
include "includes.php";
set_time_limit(6000);
?>

<!DOCTYPE html>
<html>
<head>
	<title><?php echo CLEARANCE_CATEGORIZATION; ?></title>
</head>
<style type="text/css">
	
	button#load{
    padding: 0 3em;
    outline: none;
    border: none;
    color: #fff;
    text-transform: uppercase;
    font-weight: 700;
    letter-spacing: 1px;
    font-size: 1em;
    line-height: 4;
    overflow: hidden;
    border-radius: 5px;
    background: rgba(0,0,0,0.2);
    text-align: center;
    cursor: pointer;
    margin: 20px auto;
    display: block;
}
</style>

<body>
<?php include "main_navbar.php"; ?>



<button id="load">Start</button>
<center><div id="wait" style="display:none;"><img src='http://preloaders.net/preloaders/728/Skype%20balls%20loader.gif'/><br>Proccessing...</div></center>
<center><div id="txt">
                        <h2>
                        <?php
						  if($_SESSION['all_page_go'] == 1 or !isset($_SESSION['all_page_go'])){
						  	info_msg("State: Ready");
						?>
						<script>
							$(document).ready(function(){
							    $(document).ajaxStart(function(){
							        $("#wait").css("display", "block");
							    });
							    $(document).ajaxComplete(function(){
							        $("#wait").css("display", "none");
							    });
							    $("#load").click(function(){
							        $("#txt").load("clearancecat-run.php");
							    });
							});
						</script>
						<?php
						  	exit();
						  }else{			
						  	info_msg("State: Not Connected");
						  }
						?>
</h2></div></center>
</body>
</html>
