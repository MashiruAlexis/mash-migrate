<?php
include "core.php";
include "includes.php";


?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo MIGRATIONS; ?></title>
	<link rel="stylesheet" href="css/win8.css">
</head>
<body>
<?php include "main_navbar.php"; _validateConnection(); ?>
<div class="container">
	<div class="page-header">
		<h3>Migrations</h3>
	</div>
<div class="demo-wrapper">   
 
<!--each tile should specify what page type it opens (to determine which animation) and the corresponding page name it should open-->
  <div class="dashboard clearfix">
    <ul class="tiles">
      <div class="col1 clearfix">
      <a href="<?php echo SITE_BASE_URL; ?>main.php">
        <li class="tile tile-big tile-4 fig-tile" data-page-type="r-page" data-page-name="random-r-page">
          <figure>
            <figcaption class="tile-caption caption-left">Launch Customers Migration?</figcaption>
            <h3>Customers Migration</h3>
          </figure>
        </li>
        </a>        
      </div>

      <div class="col2 clearfix">
      <a href="<?php echo SITE_BASE_URL; ?>main.php">
        <li class="tile tile-big tile-4 fig-tile" data-page-type="r-page" data-page-name="random-r-page">
          <figure>
            <figcaption class="tile-caption caption-left">Launch Orders Migration?</figcaption>
            <h3>Orders Migration</h3>
          </figure>
        </li>
       </a>     
      </div>

      <div class="col3 clearfix">
      <a href="<?php echo SITE_BASE_URL; ?>urlmigrate.php">
        <li class="tile tile-big tile-4 fig-tile" data-page-type="r-page" data-page-name="random-r-page">
          <figure>
            <figcaption class="tile-caption caption-left">Launch URL Migration?</figcaption>
            <h3>URL Migration</h3>
          </figure>
        </li>
        </a>      
      </div>

      <div class="col1 clearfix">
      <a href="<?php echo SITE_BASE_URL; ?>msrp-view.php">
        <li class="tile tile-big tile-4 fig-tile" data-page-type="r-page" data-page-name="random-r-page">
          <figure>
            <figcaption class="tile-caption caption-left">Launch MSRP Migration?</figcaption>
            <h3>MSRP Migration</h3>
          </figure>
        </li>
        </a>      
      </div>

      <div class="col2 clearfix">
      <a href="<?php echo SITE_BASE_URL; ?>update.php">
        <li class="tile tile-big tile-4 fig-tile" data-page-type="r-page" data-page-name="random-r-page">
          <figure>
            <figcaption class="tile-caption caption-left">Launch Fix Database?</figcaption>
            <h3>Fix Database</h3>
          </figure>
        </li>
       </a>       
      </div>

      <div class="col2 clearfix">
      <a href="<?php echo SITE_BASE_URL; ?>clearancecat-view.php">
        <li class="tile tile-big tile-4 fig-tile" data-page-type="r-page" data-page-name="random-r-page">
          <figure>
            <figcaption class="tile-caption caption-left">Launch Clearance Categorization?</figcaption>
            <h3>Clearance Categorization</h3>
          </figure>
        </li>
       </a>       
      </div>


    </ul>
  </div><!--end dashboard-->

</div>
</div>
</body>
</html>